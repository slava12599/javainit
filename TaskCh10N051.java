public class TaskCh10N051 {
    public static void main(String[] args) {
        System.out.println(a(5));
        System.out.println(b(5));
        System.out.println(c(5));
    }

    public static int a(int n) {
        if (n > 0) {
            return n;
        }
        return a(n - 1);
    }

    public static int b(int n) {
        if (n > 0) {
            return b(n - 1);
        }
        return n;
    }

    public static int c(int n) {
        if (n > 0) {
            return n;
        } else {
            c(n - 1);
            return n;
        }
    }
}
