public class TaskCh10N050 {
    public static void main(String[] args) {
        System.out.println(ack(1, 3));
    }

    public static int ack(int n, int m) {
        if (m == 0) {
            return 2 * n;
        } else if (m >= 1) {
            if (n == 0) {
                return 0;
            } else if (n == 1) {
                return 2;
            } else {
                return ack(m - 1, ack(m, n - 1));
            }
        }
        return n;
    }
}
