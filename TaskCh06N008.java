import java.util.Arrays;

public class TaskCh06N008 {
    public static void main(String[] args) {
        int n = 5;
        int[] a = new int[20];
        for (int i = 1; i < a.length; i++) {
            a[i] = i * i;
            if (n < a[i]) {
                break;
            }
            System.out.println(a[i]);
        }
    }
}

