import java.util.Scanner;

public class TaskCh02N031 {
    public static void main(String[] args) {
        int n = scanNumber();
        int x = findX(n);
        System.out.println(x);
    }

    private static int scanNumber() {
        Scanner scan = new Scanner(System.in);
        int number = scan.nextInt();
        return number;
    }

    private static int findX(int n) {
        for (int x = 100; x <= 999; x++) {
            if (replaceDigits(x) == n) {
                return x;
            }
        }
        return n;
    }

    private static int replaceDigits(int x) {
        int number2 = 0;
        for (int i = 0; i < 2; i++) {
            number2 *= 10;
            number2 += x % 10;
            x /= 10;
        }
        number2 += x * 100;
        return number2;
    }
}




