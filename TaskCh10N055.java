import java.io.IOException;

public class TaskCh10N055 {
    public static String getDigit(int number) {
        int N = 7;
        String[] digits = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};
        return (number == 0) ? "" : getDigit(number / N) + digits[number % N];
    }

    public static void main(String[] args) throws IOException {
        System.out.println(getDigit(15));
    }
}