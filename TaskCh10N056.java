public class TaskCh10N056 {
    public static void main(String[] args) {
        System.out.println(recursion(7));
    }

    public static boolean recursion(int n) {
        int i = 2;
        if (n < 2) {
            return false;
        } else if (n == 2) {
            return true;
        } else if (n % i == 0) {
            return false;
        } else if (i < n / 2) {
            return recursion(n + 1);
        } else {
            return true;
        }
    }
}
