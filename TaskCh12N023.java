public class TaskCh12N023 {
    public static void main(String[] args) {
        a();
        System.out.println();
        b();
        System.out.println();
        c();
    }

    public static int[][] a() {
        int insize = 7;
        int[][] a = new int[insize][insize];
        for (int i = 0; i < insize; i++) {
            for (int j = 0; j < insize; j++) {
                if (j == i) {
                    a[i][j] = 1;
                } else if (6 - i % 2 != 0) {
                    a[j][6 - j] = 1;
                } else {
                    a[i][j] = 0;
                }
            }
        }
        for (int i = 0; i < insize; i++) {
            for (int j = 0; j < insize; j++) {
                System.out.print(a[i][j] + "\t");
            }
            System.out.println();
        }
        return a;
    }


    public static int[][] b() {
        int insize = 7;
        int[][] a = new int[insize][insize];
        for (int i = 0; i < insize; i++) {
            for (int j = 0; j < insize; j++) {
                if (j == 3) {
                    a[i][j] = 1;
                }
                if (j == i) {
                    a[i][j] = 1;
                } else if (6 - i % 2 != 0) {
                    a[j][6 - j] = 1;
                }
            }
        }
        for (int i = 0; i < insize; i++) {
            for (int j = 0; j < insize; j++) {
                System.out.print(a[i][j] + "\t");
            }
            System.out.println();
        }
        return a;
    }

    public static int[][] c() {
        int insize = 7;
        int[][] a = new int[insize][insize];
        int dl = insize;
        for (int i = 0; i < insize; i++) {
            dl--;
            for (int j = 0; j < insize; j++) {
                a[i][j] = 1;
                if (i > j && j < dl) {
                    a[i][j] = 0;
                }
                if (j > i && j > dl) {
                    a[i][j] = 0;
                }
            }
        }
        for (int i = 0; i < insize; i++) {
            for (int j = 0; j < insize; j++) {
                System.out.print(a[i][j] + "\t");
            }
            System.out.println();
        }
        return a;
    }
}