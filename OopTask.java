import java.util.ArrayList;

public class OopTask {

    public static void main(String[] args) {


        ArrayList<Candidate> candidates = new ArrayList<>();

        Employer employer = new Employer();
        AbstractCandidate candidate1 = new GetJavaJobGraduate();
        AbstractCandidate candidate2 = new GetJavaJobGraduate();
        AbstractCandidate candidate3 = new GetJavaJobGraduate();
        AbstractCandidate candidate4 = new GetJavaJobGraduate();
        AbstractCandidate candidate5 = new SelfLearnerStudent();
        AbstractCandidate candidate6 = new SelfLearnerStudent();
        AbstractCandidate candidate7 = new SelfLearnerStudent();
        AbstractCandidate candidate8 = new SelfLearnerStudent();
        AbstractCandidate candidate9 = new SelfLearnerStudent();
        AbstractCandidate candidate10 = new SelfLearnerStudent();

        candidate1.setName("Alex");
        candidate2.setName("Egor");
        candidate3.setName("Danil");
        candidate4.setName("Slava");
        candidate5.setName("Ivan");
        candidate6.setName("Anton");
        candidate7.setName("Roman");
        candidate8.setName("Danil");
        candidate9.setName("Alex");
        candidate10.setName("Ivan");

        candidates.add(candidate1);
        candidates.add(candidate2);
        candidates.add(candidate3);
        candidates.add(candidate4);
        candidates.add(candidate5);
        candidates.add(candidate6);
        candidates.add(candidate7);
        candidates.add(candidate8);
        candidates.add(candidate9);
        candidates.add(candidate10);

        for (Candidate candidate : candidates) {
            employer.hello();
            candidate.hello();
            candidate.describeExperience();
        }
    }
}

interface Person {

    void hello();
}

interface Candidate extends Person {

    void describeExperience();
}

abstract class AbstractCandidate implements Candidate {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void hello() {

        System.out.println("Hi my name is " + name);
    }
}

class GetJavaJobGraduate extends AbstractCandidate {

    @Override
    public void describeExperience() {
        System.out.println("I passed successfully getJavaJob exams and code reviews.");
    }
}

class SelfLearnerStudent extends AbstractCandidate {

    @Override
    public void describeExperience() {
        System.out.println("I have been learning Java by myself, nobody examined how thorough is my knowledge and how good is my code.");
    }
}

class Employer implements Person {

    @Override
    public void hello() {
        System.out.println("Hi! Introduce yourself and describe your java experience please");
    }
}