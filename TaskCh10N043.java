public class TaskCh10N043 {
    public static void main(String[] args) {
        System.out.println(recursion(1231854));
        System.out.println(recursion0(234));
    }

    public static int recursion(int n) {
        int sun;
        if (n <= 10) {
            return n;
        } else {
            return n % 10 + recursion(n / 10);
        }
    }

    public static int recursion0(int a) {
        if (a <= 0) {
            return a;
        } else
        return recursion0(a / 10) + 1;
    }
}


