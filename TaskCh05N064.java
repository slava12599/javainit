public class TaskCh05N064 {
    public static void main(String[] args) {
        int[][] region = {
                {100, 1300},
                {23, 250},
                {420, 5320},
                {5, 150},
                {14, 230},
                {345, 10000},
                {234, 2356},
                {32, 235},
                {10, 2564},
                {482, 548},
                {464, 500},
                {40, 547}
        };
        int a = averagePopulationDensityByRegion(region);
        System.out.println(a);
    }

    public static int averagePopulationDensityByRegion(int[][] a) {
        int area;
        int b = 0;
        int populationDensity = 1000;
        for (int i = 1; i < a.length; i++) {
            area = 0;
            populationDensity = 1000;

            for (int j = 1; j < a.length; j++) {
                populationDensity *= a[i][0];
                area += a[i][j];
                b = populationDensity / area;
                return b;
            }
            System.out.println(b);
        }
        return b;
    }
}

