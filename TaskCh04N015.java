import java.util.Scanner;

public class TaskCh04N015 {
    public static void main(String[] args) {
        System.out.println(scanNumber());
    }

    private static int scanNumber() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите текущий год ");
        int num1 = scan.nextInt();
        System.out.println("Введите текущий месяц");
        int num2 = scan.nextInt();
        System.out.println("Введите свой год рождения");
        int num3 = scan.nextInt();
        System.out.println("Введите свой месяц рождения");
        int num4 = scan.nextInt();
        int number = ageDetermination(num1, num2, num3, num4);
        return number;
    }

    private static int ageDetermination(int thisYear, int thisMonth0, int yearOfBirth, int month1OfBirth) {
        int age = 0;
        for (int j = thisYear; j > yearOfBirth; j--) {
            for (int i = 1; i <= month1OfBirth; i++) {
                if (i == month1OfBirth) {
                    age++;
                }
            }
        }
        if (thisMonth0 < month1OfBirth) {
            age -= 1;
        }
        return age;
    }
}

