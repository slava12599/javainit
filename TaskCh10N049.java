public class TaskCh10N049 {
    public static void main(String[] args) {
        int[] a = {123, 124, 1000, 2, 45, 577};
        int d = maxIndex(a);
        System.out.println(d);
    }

    private static int maxIndex0(int[] arr, int start) {
        if (isMaxElement(arr, arr[start]))
            return start;
        return maxIndex0(arr, start + 1);
    }

    private static boolean isMaxElement(int[] arr, int element) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > element)
                return false;
        }
        return true;
    }

    public static int maxIndex(int[] arr) {
        return maxIndex0(arr, 0);
    }
}




