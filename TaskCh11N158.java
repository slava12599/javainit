public class TaskCh11N158 {
    public static void main(String[] args) {
        int[] t = {1, 4, 3, 1, 3, 5, 6, 8};
        delete(t);
        for (int i = 0; i < t.length; i++) {
            System.out.println(t[i]);
        }
    }

    public static int[] delete(int[] inp) {
        for (int i = 0; i < inp.length; i++) {
            for (int j = i + 1; j < inp.length; j++) {
                if (inp[i] == inp[j]) {
                    j--;
                    for (int k = j; k < inp.length - 1; k++) {
                        inp[k] = inp[k + 1];
                    }
                    inp[inp.length - 1] = 0;
                }
            }
        }
        return inp;
    }
}
