public class TaskCh03N026 {
    public static void main(String[] args) {
        boolean a = a(false, false, true);
        System.out.println(a);
        boolean b = b(false, false, false);
        System.out.println(b);
        boolean v = v(true, false, true);
        System.out.println(v);
    }

    private static boolean a(boolean x, boolean y, boolean z) {
        return !(x || y) && (!x || !z);
    }

    private static boolean b(boolean x, boolean y, boolean z) {
        return !(!x && y) || (x && !z);
    }

    private static boolean v(boolean x, boolean y, boolean z) {
        return x || !y && !(x || !z);
    }
}
