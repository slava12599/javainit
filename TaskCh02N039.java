public class TaskCh02N039 {
    public static void main(String[] args) {
        double a = getTheClockwiseAngle(12, 00);
        double b = getTheClockwiseAngle(8, 45);
        System.out.println(a - b);
    }

    public static double getTheClockwiseAngle(double h, double m) {
        double fractionsOfAnHour = h + m / 60;
        double clockwiseAngle = fractionsOfAnHour * 360 / 12;
        return clockwiseAngle;

    }
}
