import java.util.Scanner;

public class TaskCh04N115 {
    public static void main(String[] args) {
        int a;
        String result = "";
        Scanner in = new Scanner(System.in);
        System.out.println("Введите год ");
        a = in.nextInt();
        int animal = a % 12;
        int color = a % 10;
        switch (color) {
            case 0:
            case 1:
                result = "Белый ";
                break;
            case 2:
            case 3:
                result = "Черный ";
                break;
            case 4:
            case 5:
                result = "Зеленый ";
                break;
            case 6:
            case 7:
                result = "Красный ";
                break;
            case 8:
            case 9:
                result = "Желтый ";
                break;
        }
        switch (animal) {
            case 0:
                result += "обезьяна ";
                break;
            case 1:
                result += "петух ";
                break;
            case 2:
                result += "собака ";
                break;
            case 3:
                result += "свинья ";
                break;
            case 4:
                result += "крыса ";
                break;
            case 5:
                result += "бык ";
                break;
            case 6:
                result += "тигр ";
                break;
            case 7:
                result += "заяц ";
                break;
            case 8:
                result += "дракон ";
                break;
            case 9:
                result += "змея ";
                break;
            case 10:
                result += "лошадь ";
                break;
            case 11:
                result += "овца ";
                break;
        }
        System.out.println(result);
    }
}
