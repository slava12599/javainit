import java.util.Scanner;

public class TaskCh06N087 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Game start = new Game();
        start.play();
    }
}

class Game {
    Scanner scan = new Scanner(System.in);
    String team1;
    String team2;
    int scoreTeam1;
    int scoreTeam2;
    int a = 1;

    Game() {
        this.team1 = team1;
        this.team2 = team2;
        this.scoreTeam1 = scoreTeam1;
        this.scoreTeam2 = scoreTeam2;
    }

    void play() {
        System.out.println("Enter team1:");
        team1 = scan.next();
        System.out.println("Enter team2:");
        team2 = scan.next();
        while (a > 0) {
            System.out.println("Enter team to score(1 or 2 or 0 to finish game:");
            a = scan.nextInt();
            switch (a) {
                case 1:
                    System.out.println("Enter score (1 or 2 or 3)");
                    scoreTeam1 += scan.nextInt();
                    System.out.println(score());
                    break;
                case 2:
                    System.out.println("Enter score (1 or 2 or 3)");
                    scoreTeam2 += scan.nextInt();
                    System.out.println(score());
                    a--;
                    break;
            }
        }
        if (a == 0) {
            System.out.println(result() + "\n" + score());
            return;
        }
    }

    String score() {
        String rpovejScore1 = team1 + ":" + scoreTeam1;
        String promejScore2 = team2 + ":" + scoreTeam2;
        String d = rpovejScore1 + "\n" + promejScore2;

        return d;
    }

    String result() {
        String a, b;
        if (scoreTeam1 > scoreTeam2) {
            a = "win" + " " + team1 + "\n" + "Lose" + " " + team2;
        } else {
            a = "Win:" + team2 + "\n" + "Lose:" + team1;
        }
        return a;
    }
}