public class TaskCh02N013 {
    public static void main(String[] args) {
        int number1 = 143;
        int number2 = 0;
        for (int i = 0; i < 3; i++) {
            number2 *= 10;
            number2 += number1 % 10;
            number1 /= 10;
        }
        System.out.println(number2);
    }
}
