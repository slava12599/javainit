public class TaskCh04N033 {
    public static void main(String[] args) {
        int naturalNumber = 13;
        if (naturalNumber % 2 == 0) {
            System.out.println("Данное число заканчивается на четное число");
        } else {
            System.out.println("Данное число заканчивается на нечетное число");
        }
    }
}
