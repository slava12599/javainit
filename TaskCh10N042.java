public class TaskCh10N042 {
    public static void main(String[] args) {

        System.out.println(pow(6, 2));
    }

    private static int pow(int x, int n) {
        if (n == 0)
            return 1;
        else if (n == 1)
            return x;
        else if (n % 2 == 0)
            return pow(x * x, n / 2);
        else
            return pow(x * x, n / 2) * x;
    }
}