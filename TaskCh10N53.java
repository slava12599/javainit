import java.util.Arrays;

public class TaskCh10N53 {
    public static void main(String[] args) {
        int[] input = {1, 2, 3, 4, 5};
        mirarr(input);
        System.out.println(Arrays.toString(input));
    }

    public static int[] mirarr(int[] x) {
        int a = 0;
        for (int i = 0; i < x.length / 2; i++) {
            a = x[i];
            x[i] = x[x.length - 1 - i];
            x[x.length - 1 - i] = a;
        }
        return x;
    }
}
