public class TaskCh10N044 {
    public static void main(String[] args) {

        System.out.println(recursion0(23452));
    }

    private static int recursion0(int a) {
        int sum = recursion(a);
        int c;
        int b;
        c = sum % 10;
        b = sum / 10;
        int number = c + b;
        if (number < 10) {
            return number;
        } else
            number = 1;
        return number;
    }

    private static int recursion(int n) {
        int sun;
        if (n <= 10) {
            return n;
        } else {
            sun = n % 10 + recursion(n / 10);
        }
        return sun;
    }
}