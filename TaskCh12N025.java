public class TaskCh12N025 {
    public static void main(String[] args) {
        outpat();
    }

    public static int[][] outpat() {
        int[][] arr;
        int insize0 = 12;
        int insize1 = 10;
        arr = a1();
        for (int i = 0; i < insize0; i++) {
            for (int j = 0; j < insize1; j++) {
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
        arr = a2();
        for (int i = 0; i < insize0; i++) {
            for (int j = 0; j < insize1; j++) {
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
        arr = a3();
        for (int i = 0; i < insize0; i++) {
            for (int j = 0; j < insize1; j++) {
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
        arr = a4();
        for (int i = 0; i < insize0; i++) {
            for (int j = 0; j < insize1; j++) {
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
        arr = a5();
        for (int i = 0; i < insize0; i++) {
            for (int j = 0; j < insize1; j++) {
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
        arr = a6();
        for (int i = 0; i < insize0; i++) {
            for (int j = 0; j < insize1; j++) {
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
        arr = a7();
        for (int i = 0; i < insize0; i++) {
            for (int j = 0; j < insize1; j++) {
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
        arr = a8();
        for (int i = 0; i < insize0; i++) {
            for (int j = 0; j < insize1; j++) {
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
        arr = a9();
        for (int i = 0; i < insize0; i++) {
            for (int j = 0; j < insize1; j++) {
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
        arr = a10();
        for (int i = 0; i < insize0; i++) {
            for (int j = 0; j < insize1; j++) {
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
        arr = a11();
        for (int i = 0; i < insize0; i++) {
            for (int j = 0; j < insize1; j++) {
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
        arr = a12();
        for (int i = 0; i < insize0; i++) {
            for (int j = 0; j < insize1; j++) {
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
        arr = a13();
        for (int i = 0; i < insize0; i++) {
            for (int j = 0; j < insize1; j++) {
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
        arr = a14();
        for (int i = 0; i < insize0; i++) {
            for (int j = 0; j < insize1; j++) {
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
        arr = a15();
        for (int i = 0; i < insize0; i++) {
            for (int j = 0; j < insize1; j++) {
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
        arr = a16();
        for (int i = 0; i < insize0; i++) {
            for (int j = 0; j < insize1; j++) {
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
        return arr;
    }

    public static int[][] a16() {
        int insize0 = 12;
        int insize1 = 10;
        int r = 1;
        int b = 1;
        int a[][] = new int[insize0][insize1];
        for (int j = insize1 - 1; j >= 0; j--) {
            if (j % 2 == 0) {
                for (int i = 0; i < insize0; i++) {
                    a[i][j] = r;
                    r++;
                }
            } else if (j % 2 != 0) {
                for (int i = insize0 - 1; i >= 0; i--) {
                    a[i][j] = r;
                    r++;
                }
            }
        }
        return a;
    }

    public static int[][] a15() {
        int insize0 = 12;
        int insize1 = 10;
        int r = 1;
        int b = 1;
        int a[][] = new int[insize0][insize1];
        for (int i = insize0 - 1; i >= 0; i--) {
            if (i % 2 == 0) {
                for (int j = 0; j < insize1; j++) {
                    a[i][j] = r;
                    r++;
                }
            } else if (i % 2 != 0) {
                for (int j = insize1 - 1; j >= 0; j--) {
                    a[i][j] = r;
                    r++;
                }
            }
        }
        return a;
    }

    public static int[][] a14() {
        int insize0 = 12;
        int insize1 = 10;
        int r = 1;
        int b = 1;
        int a[][] = new int[insize0][insize1];
        for (int j = 0; j < insize1; j++) {
            if (j % 2 != 0) {
                for (int i = 0; i < insize0; i++) {
                    a[i][j] = r;
                    r++;
                }
            } else if (j % 2 == 0) {
                for (int i = insize0 - 1; i >= 0; i--) {
                    a[i][j] = r;
                    r++;
                }
            }
        }
        return a;
    }

    public static int[][] a13() {
        int insize0 = 12;
        int insize1 = 10;
        int r = 1;
        int b = 1;
        int a[][] = new int[insize0][insize1];
        for (int j = insize1 - 1; j >= 0; j--) {
            if (j % 2 != 0) {
                for (int i = 0; i < insize0; i++) {
                    a[i][j] = r;
                    r++;
                }
            } else if (j % 2 == 0) {
                for (int i = insize0 - 1; i >= 0; i--) {
                    a[i][j] = r;
                    r++;
                }
            }
        }
        return a;
    }

    public static int[][] a12() {
        int insize0 = 12;
        int insize1 = 10;
        int r = 1;
        int b = 1;
        int a[][] = new int[insize0][insize1];
        for (int i = 0; i < insize0; i++) {
            if (i % 2 == 0) {
                for (int j = insize1 - 1; j >= 0; j--) {
                    a[i][j] = r;
                    r++;
                }
            } else if (i % 2 != 0) {
                for (int j = 0; j < insize1; j++) {
                    a[i][j] = r;
                    r++;
                }
            }
        }
        return a;
    }

    public static int[][] a11() {
        int insize0 = 12;
        int insize1 = 10;
        int r = 1;
        int b = 1;
        int a[][] = new int[insize0][insize1];
        for (int i = insize0 - 1; i >= 0; i--) {
            if (i % 2 != 0) {
                for (int j = 0; j < insize1; j++) {
                    a[i][j] = r;
                    r++;
                }
            } else if (i % 2 == 0) {
                for (int j = insize1 - 1; j >= 0; j--) {
                    a[i][j] = r;
                    r++;
                }
            }
        }
        return a;
    }

    public static int[][] a10() {
        int insize0 = 12;
        int insize1 = 10;
        int r = 1;
        int b = 1;
        int a[][] = new int[insize0][insize1];
        for (int j = insize1 - 1; j >= 0; j--) {
            for (int i = insize0 - 1; i >= 0; i--) {
                b = r;
                r++;
                a[i][j] = b;
            }
        }
        return a;
    }

    public static int[][] a9() {
        int insize0 = 12;
        int insize1 = 10;
        int r = 1;
        int b = 1;
        int a[][] = new int[insize0][insize1];
        for (int i = insize0 - 1; i >= 0; i--) {
            for (int j = insize1 - 1; j >= 0; j--) {
                b = r;
                r++;
                a[i][j] = b;
            }
        }
        return a;
    }

    public static int[][] a8() {
        int insize0 = 12;
        int insize1 = 10;
        int r = 1;
        int b = 1;
        int a[][] = new int[insize0][insize1];
        for (int j = insize1 - 1; j >= 0; j--) {
            for (int i = 0; i < insize0; i++) {
                a[i][j] = r;
                r++;
            }
        }
        return a;
    }

    public static int[][] a7() {
        int insize0 = 12;
        int insize1 = 10;
        int r = 1;
        int b = 1;
        int a[][] = new int[insize0][insize1];
        for (int i = insize0 - 1; i >= 0; i--) {
            for (int j = 0; j < insize1; j++) {
                a[i][j] = r;
                r++;
            }
        }
        return a;
    }

    public static int[][] a6() {
        int insize0 = 12;
        int insize1 = 10;
        int r = 1;
        int b = 1;
        int a[][] = new int[insize0][insize1];
        for (int j = 0; j < insize1; j++) {
            if (j % 2 == 0) {
                for (int i = 0; i < insize0; i++) {
                    a[i][j] = r;
                    r++;
                }
            } else if (j % 2 != 0) {
                for (int i = insize0 - 1; i >= 0; i--) {
                    a[i][j] = r;
                    r++;
                }
            }
        }
        return a;
    }

    public static int[][] a5() {
        int insize0 = 12;
        int insize1 = 10;
        int r = 1;
        int b = 1;
        int a[][] = new int[insize0][insize1];
        for (int i = 0; i < insize0; i++) {
            if (i % 2 == 0) {
                for (int j = 0; j < insize1; j++) {
                    a[i][j] = r;
                    r++;
                }
            } else if (i % 2 != 0) {
                for (int j = insize1 - 1; j >= 0; j--) {
                    a[i][j] = r;
                    r++;
                }
            }
        }
        return a;
    }

    public static int[][] a4() {
        int insize0 = 12;
        int insize1 = 10;
        int r = 1;
        int b = 1;
        int a[][] = new int[insize0][insize1];
        for (int j = 0; j < insize1; j++) {
            for (int i = insize0 - 1; i >= 0; i--) {
                a[i][j] = r;
                r++;
            }
        }
        return a;
    }

    public static int[][] a3() {
        int insize0 = 12;
        int insize1 = 10;
        int r = 0;
        int b = 1;
        int a[][] = new int[insize0][insize1];
        for (int i = 0; i < insize0; i++) {
            for (int j = insize1 - 1; j >= 0; j--) {
                a[i][j] = b;
                b++;
            }
        }
        return a;
    }

    public static int[][] a2() {
        int insize0 = 12;
        int insize1 = 10;
        int r = 1;
        int b = 0;
        int a[][] = new int[insize0][insize1];
        for (int j = 0; j < insize1; j++) {
            for (int i = 0; i < insize0; i++) {
                a[i][j] = r;
                r++;
            }
        }
        return a;
    }

    public static int[][] a1() {
        int insize0 = 12;
        int insize1 = 10;
        int r = 1;
        int b = 0;
        int[][] a = new int[insize0][insize1];
        for (int i = 0; i < insize0; i++) {
            for (int j = 0; j < insize1; j++) {
                b = r + j;
                a[i][j] = b;
            }
            r += insize1;
        }
        return a;
    }
}