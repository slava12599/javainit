import java.util.Scanner;

public class TaskCh10N046 {
    public static void main(String[] args) {
        System.out.println(recursion(2, 3, 5));
        int firstM, k, n, i = 1, sum, nM;
        System.out.println("Введите первый член прогрессии");
        Scanner in = new Scanner(System.in);
        firstM = in.nextInt();
        System.out.println("Введите знаменатель прогрессии");
        k = in.nextInt();
        System.out.println("Введите n");
        n = in.nextInt();
        in.close();
        nM = firstM * k;
        sum = firstM + nM;
        while (i < n - 1) {
            nM = nM * k;
            sum = sum + nM;
            i++;
        }
        System.out.println("N-ый член прогрессии: " + nM);
        System.out.println("Сумма n-ых членов прогрессии: " + sum);
    }

    public static int recursion(int firstMember, int denominator, int n) {
        n--;
        int mn;
        int sum;
        mn =  firstMember + denominator;
        if (1 < n) {
            return mn;
        } else {
            mn =  firstMember + denominator;
            return mn + recursion( firstMember * denominator, denominator, n);
        }
    }
}

