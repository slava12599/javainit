import java.util.Scanner;
public class TaskCh10N045 {
    public static void main(String[] args) {
        System.out.println(recursion(2, 3, 5));
        int firstM, k, n, sum, nM;
        int i = 1;
        System.out.println("Введите первый член прогрессии");
        Scanner in = new Scanner(System.in);
        firstM = in.nextInt();
        System.out.println("Введите разность прогрессии");
        k = in.nextInt();
        System.out.println("Введите n");
        n = in.nextInt();
        in.close();
        nM = firstM + k;
        sum = firstM + nM;
        while (i < n - 1) {
            nM = nM + k;
            sum = sum + nM;
            i++;
        }
        System.out.println("N-ый член прогрессии: " + nM);
        System.out.println("Сумма n-ых членов прогрессии: " + sum);
    }

    public static int recursion(int firstMember, int difference, int n) {
        n--;
        int mn;
        int sum;
        mn = firstMember + difference;
        if (1 < n) {
            return mn;
        } else {
            mn = firstMember + difference;
            return mn + recursion(firstMember + difference, difference, n);
        }
    }
}