import java.time.LocalDate;
import java.util.ArrayList;
import java.util.stream.Stream;

public class TaskCh13N012 {
    public static void main(String[] args) {

        Database db1 = new Database();
        db1.addEmployee(new Employee("Альберт", "Иванов", "Иванович", "Омск", 2015, 12));
        db1.addEmployee(new Employee("Иван", "Сидоров", "Сидорович", "Кемерово", 2008, 9));
        db1.addEmployee(new Employee("Антон", "Петров", "Владимирович", "Москва", 2017, 1));
        db1.addEmployee(new Employee("Денис", "Хворостинин", "Иванович", "Питер", 2014, 7));
        db1.addEmployee(new Employee("Даниил", "Иванов", "Дмитриевич", "Москва", 2013, 6));
        db1.addEmployee(new Employee("Михаил", "Петраков", "Евгеньевич", "Москва", 2018, 11));
        db1.addEmployee(new Employee("Владимир", "Жебровский", "Владимирович", "Кемерово", 2012, 11));
        db1.addEmployee(new Employee("Дмитрий", "Сидоров", "Андреевич", "Питер", 2011, 11));
        db1.addEmployee(new Employee("Алексей", "Иванов", "Алексеевич", "Москва", 2019, 1));
        db1.addEmployee(new Employee("Игорь", "Краснов", "Дмитриевич", "Омск", 2012, 7));


        ArrayList<Employee> myLocalDatabase = new ArrayList<>();
        for (Employee employee : db1.getDb()) {
            if (employee.getWorkingYears() >3) {
               myLocalDatabase.add(employee);
                for (int i = 0; i<myLocalDatabase.size();i++){
                }
            }
        }
        System.out.println(myLocalDatabase.toString());
    }
}

class Database {
    private ArrayList<Employee> db;

    Database() {
        db = new ArrayList<>();
    }

    public void addEmployee(Employee empl) {
        db.add(empl);
    }

    public ArrayList<Employee> getDb() {
        return db;
    }
}

class Employee {
    private String name;
    private String patronymic;
    private String surname;
    private String address;
    private int year;
    private int month;

    Employee(String name, String patronymic, String surname, String address, int year, int month) {
        this.name = name;
        this.patronymic = patronymic;
        this.surname = surname;
        this.address = address;
        this.year = year;
        this.month = month;
    }

    public String toString() {
        return String.format("\nИмя: %s, Фамилия: %s, Отчество: %s\nГород: %s, Год: %d, Месяц: %d" ,
                name, patronymic, surname, address, year, month);
    }

    public int getWorkingYears() {
        LocalDate now = LocalDate.now();
        now.getYear();
        now.getMonth().getValue();
        return month&year;
    }
}


